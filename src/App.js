import React, { useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';
import styled from 'styled-components';
import Grid from './ui/components/Grid/Grid';
import useComponentDimensions from './core/hooks/useComponentDimensions';
import { setDimensions } from './core/actions/generalActions';
import GameControls from './ui/pages/GameControls/GameControls';
import Nav from './ui/components/commons/Nav/Nav';
import Stats from './ui/components/Stats/Stats';
import LevelStats from './ui/components/Stats/PlayerStats/LevelStats';
import Paths from './core/utils/paths';

const StyledApp = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin-top: 60px;
    height: 100%;
    padding: 10px;`;

export default function App() {
	const dispatch = useDispatch();

	const appRef = useRef();

	const { width, height } = useComponentDimensions(appRef);

	useEffect(() => {
		dispatch(setDimensions({ width, height }));
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [width, height]);

	return (
		<StyledApp ref={appRef}>
			<Nav />
			<Switch>
				<Route exact path={Paths.HOME} render={() => <Grid />} />
				<Route exact path={Paths.SETTINGS} component={GameControls} />
				<Route exact path={Paths.STATS} component={Stats} />
				<Route exact path={Paths.LEVELS} component={LevelStats} />
				<Redirect to={Paths.HOME} />
			</Switch>
		</StyledApp>
	);
}

import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import styled from 'styled-components';
import { setFinish, setRows, setStart } from '../../../core/actions/gameControllsActions';
import Input from '../../components/commons/Input/Input';
import Button from '../../components/commons/Button/Button';
import PlayerSettings from '../../components/settings/PlayerSettings/PlayerSettings';

const GameControlsContainer = styled.section`
    display: flex;
    align-content: center;
    flex-direction: column;
    font-size: 20px;
    width: 75%;
    user-select: none;
    font-size: 2vw;
`;

const InputGroup = styled.section`
    display: flex;
    flex-direction: column;
    justify-items: center;
    align-items: center;
    margin: 10px;
    padding: 5px;
    border-bottom: 5px dotted coral;
`;

const SectionTitle = styled.h2`
    font-size: 4vw;
`;
const GameControls = () => {
	const dispatch = useDispatch();

	const { rows, startNode: sNode, finishNode: fNode } = useSelector(state => ({
		rows: state.gameControls.rows,
		startNode: state.gameControls.startNode,
		finishNode: state.gameControls.finishNode,
	}));

	const [formData, setFormData] = useState({
		startNode: { startRow: sNode.row, startCol: sNode.col },
		endNode: { endRow: fNode.row, endCol: fNode.col },
		min: 0,
		max: rows - 1,
		step: 1,
	});

	const handleMapChange = (event) => {
		const {
			name, value, min, max,
		} = event.target;
		if (name === 'rows' && +value >= +min && +value <= +max) {
			setFormData(prevState => ({
				...prevState,
				max: +value - 1,
			}));
			dispatch(setRows(+value));
		}
	};

	const handleNodeChanges = (event, nodeName) => {
		const { name, value } = event.target;
		setFormData(prevState => ({
			...prevState,
			[nodeName]: ({
				...prevState[nodeName],
				[name]: +value,
			}),
		}));
	};

	const handleSubmit = (event) => {
		event.preventDefault();
		const {
			startNode, endNode, min, max,
		} = formData;
		const isStartRowValid = startNode.startRow >= min && startNode.startRow <= max;
		const isStartColValid = startNode.startCol >= min && startNode.startCol <= max;

		const isEndRowValid = endNode.endRow >= min && endNode.endRow <= max;
		const isEndColValid = endNode.endCol >= min && endNode.endCol <= max;

		if (isStartRowValid && isStartColValid) {
			dispatch(setStart({ row: startNode.startRow, col: startNode.startCol }));
		}

		if (isEndColValid && isEndRowValid) {
			dispatch(setFinish({ row: endNode.endRow, col: endNode.endCol }));
		}
	};

	return (
		<GameControlsContainer>
			<form onSubmit={handleSubmit}>
				<InputGroup>
					<SectionTitle>Configure map dimensions</SectionTitle>
					<Input
						id="rows"
						name="rows"
						value={rows}
						type="number"
						label="Size"
						min={10}
						max={25}
						step={formData.step}
						onChange={handleMapChange}
					/>
				</InputGroup>
				<InputGroup>
					<SectionTitle>Configure Start Position</SectionTitle>
					<Input
						name="startRow"
						id="startRow"
						value={formData.startNode.startRow}
						type="number"
						label="Start Row"
						min={formData.min}
						step={formData.step}
						max={formData.max}
						onChange={e => handleNodeChanges(e, 'startNode')}
					/>
					<Input
						id="startCol"
						name="startCol"
						value={formData.startNode.startCol}
						type="number"
						label="Start Col"
						min={formData.min}
						max={formData.max}
						step={formData.step}
						onChange={e => handleNodeChanges(e, 'startNode')}
					/>
				</InputGroup>
				<InputGroup>
					<SectionTitle> Configure End Position </SectionTitle>
					<Input
						id="endRow"
						name="endRow"
						value={formData.endNode.endRow}
						type="number"
						label="End Row"
						min={formData.min}
						max={formData.max}
						onChange={e => handleNodeChanges(e, 'endNode')}
						step={formData.step}
					/>
					<Input
						id="endCol"
						name="endCol"
						value={formData.endNode.endCol}
						type="number"
						label="End Col"
						min={formData.min}
						max={formData.max}
						onChange={e => handleNodeChanges(e, 'endNode')}
						step={formData.step}
					/>
				</InputGroup>
				<Button text="Apply Maze Changes" primary type="submit" />
			</form>
			<PlayerSettings />
		</GameControlsContainer>
	);
};

export default GameControls;

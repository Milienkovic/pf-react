import React from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components';

const buttonPressedAnimation = keyframes`
    0% {
        transform: scale(0.9);
    }
    50% {
        transform: scale(1.1);
    }
    100% {
        transform: scale(1);
    }
`;

const manageButtonColor = (props) => {
	if (props.primary) return 'blue';
	if (props.disabled) return 'grey';
	if (props.warning) return 'orange';
	if (props.danger) return 'red';
	if (props.dark) return 'rgb(168, 168, 168)';
	return 'green';
};

const StyledButton = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 10px;
    border-radius: 10px;
    min-width: 100px;
    font-size: 2vw;
    background-color: ${props => manageButtonColor(props)} ;
    border: none;
    color: white;
    font-weight: bold;
    &:hover{
        color: black;
        cursor: pointer;
    }
    &:active{
    animation-name: ${buttonPressedAnimation};
    animation-duration: 0.1s;
    }
`;

export default function Button(props) {
	const {
		text, onClick, disabled, primary, danger, warning, dark, type,
	} = props;
	return (
		<StyledButton
			disabled={disabled}
			danger={danger}
			warning={warning}
			type={type}
			dark={dark}
			primary={primary}
			onClick={onClick}
		>
			{text}
		</StyledButton>
	);
}

Button.propTypes = {
	text: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
	onClick: PropTypes.func,
	disabled: PropTypes.bool,
	primary: PropTypes.bool,
	danger: PropTypes.bool,
	warning: PropTypes.bool,
	dark: PropTypes.bool,
	type: PropTypes.string,
};

Button.defaultProps = {
	text: '',
	onClick: undefined,
	disabled: false,
	primary: false,
	danger: false,
	warning: false,
	dark: false,
	type: 'button',
};

import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';

const StyledBackdrop = styled.div`
    width: 100%;
    height: 100%;
    position: fixed;
    z-index: 100;
    left: 0;
    top: 0;
    background: rgba(140, 140, 140, 0.75);
`;

const Backdrop = (props) => {
	const content = props.display && <StyledBackdrop onClick={props.clicked} />;
	return ReactDOM.createPortal(content, document.getElementById('backdrop-hook'));
};

export default Backdrop;

import React from 'react';
import ReactDOM from 'react-dom';
import styled, { keyframes } from 'styled-components';
import PropTypes from 'prop-types';
import Backdrop from './Backdrop/Backdrop';

const modalEnterAnimation = keyframes`
    0% {
        transform: scale(0.7);
    }
    50% {
        transform: scale(1.4);
    }
    100% {
        transform: scale(1);
    }
`;

const ModalHeader = styled.header`
    text-align: center;
    padding: 0.5rem 0.25rem;
    background: #2a006e;
    color: white;
    font-size: 2.5vw;
`;

const ModalFooter = styled.footer`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    padding: 1rem 0.5rem;
    justify-items: center;
`;

const ModalContainer = styled.div`
    animation-name: ${modalEnterAnimation};
    animation-duration: 0.5s;
    z-index: 101;
    position: fixed;
    top: 22vh;
    left: 10%;
    width: 80%;
    background: rgba(255,255,255, 0.6);
    font-size: 1.5vw;
    box-shadow: 0 2px 8px rgba(0, 0, 0, 0.26);
    border-radius: 8px;
    min-height: min-content;
    max-height: 70vh;
    overflow-y: scroll;
    @media (min-width: 768px) {
    
    }
`;

const ModalBody = styled.div`
    text-align: center;
    padding: 1rem 0.5rem;
    height: max-content;
`;

const ModalOverlay = (props) => {
	const content = (
		<ModalContainer style={props.style}>
			<ModalHeader><h2>{props.header}</h2></ModalHeader>
			<ModalBody>{props.children}</ModalBody>
			<ModalFooter>
				{props.footer}
			</ModalFooter>
		</ModalContainer>
	);
	return ReactDOM.createPortal(content, document.getElementById('modal-hook'));
};
const Modal = (props) => {
	const mountModal = () => {
		if (props.display) {
			return (
				<>
					<Backdrop display={props.display} clicked={props.clicked} />
					{/* eslint-disable-next-line react/jsx-props-no-spreading */}
					<ModalOverlay {...props} />
				</>
			);
		}
		return <></>;
	};
	return (
		<>
			{mountModal()}
		</>
	);
};

Modal.propTypes = {
	display: PropTypes.bool,
	clicked: PropTypes.func,
};

Modal.defaultProps = {
	display: false,
	clicked: undefined,
};

export default Modal;

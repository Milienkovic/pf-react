import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledInput = styled.input`
    padding: 10px;
    border-radius: 5px;
    font-size: 2.5vw;
    font-weight: bold;
    text-shadow: 2px 2px white;
    background: transparent;
    max-width: 8vw;
    min-width: 2vw;
`;

const InputContainer = styled.div`
    background: rgba(255,255,255, 0.2);
    width: 90%;
    display: flex;
    align-items: center;
    justify-content: space-around;`;

const Label = styled.label`
    font-weight: bold;
`;

const Spacer = styled.span`
    flex-grow: 1;
`;

const Input = (props) => {
	const {
		min, max, onChange, step, id, name, value, type, label,
	} = props;
	return (
		<InputContainer>
			<Label htmlFor={id}>{label}</Label>
			<Spacer />
			<StyledInput
				id={id}
				name={name}
				value={value}
				type={type}
				min={min}
				max={max}
				onChange={onChange}
				step={step}
			/>
		</InputContainer>
	);
};

Input.propTypes = {
	min: PropTypes.number,
	max: PropTypes.number,
	onChange: PropTypes.func,
	id: PropTypes.string,
	name: PropTypes.string,
	step: PropTypes.number,
	value: PropTypes.number,
	type: PropTypes.string,
	label: PropTypes.string,
};

Input.defaultProps = {
	step: 1,
	min: 10,
	max: 25,
	name: '',
	id: '',
	value: 10,
	type: '',
	label: '',
	onChange: undefined,
};

export default Input;

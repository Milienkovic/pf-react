import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import Paths from '../../../../core/utils/paths';

const NavContainer = styled.nav`
  position: fixed;
  padding: 5px;
  top: 0;
  left: 0;
  z-index: 1000;
  width: 100%;
  height: 60px;
  background-color: #1167b1;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-size: 2vw;
  font-weight: bold;
`;

const Spacer = styled.span`
  margin-left: 5px;
  height: 50%;
  border-left: 1px solid white;
`;

const Ul = styled.ul`
  display: flex;
  align-items: center;
  height: 100%;
  list-style: none;
  margin-right: 20px;
`;

const NavSection = styled.section`
  display: flex;
  align-items: center;
  height: 100%;
`;

const Li = styled.li`
  display: flex;
  align-items: center;
  height: 100%;
  padding: 0px 10px 0px 10px;
`;

const NavigationLink = styled(Link)`
  display: flex;
  align-items: center;
  height: 100%;
  text-decoration: none;
  color: ${props => (props.disabled ? 'grey' : 'white')};
`;

const Logo = styled.span`
    margin-right: 10px
`;

export default function Nav() {
	const { disabled } = useSelector(state => ({
		disabled: state.gameControls.isManualPlaying || state.gameControls.isAutoPlaying,
	}));

	const handleNavClicked = (event) => {
		if (disabled) {
			event.preventDefault();
		}
	};

	return (
		<NavContainer>
			<NavSection>
				<NavigationLink to={Paths.HOME}>
					<Logo>Maze Runner</Logo>
				</NavigationLink>
				<Spacer>&nbsp;</Spacer>
			</NavSection>
			<Ul>
				<Li>
					<NavigationLink
						to={Paths.STATS}
						onClick={handleNavClicked}
						disabled={disabled}
					>
						Stats
					</NavigationLink>
				</Li>
				<Spacer>&nbsp;</Spacer>
				<Li>
					<Li>
						<NavigationLink
							to={Paths.SETTINGS}
							onClick={handleNavClicked}
							disabled={disabled}
						>
							Settings
						</NavigationLink>
					</Li>
				</Li>
			</Ul>
		</NavContainer>
	);
}

import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import styled from 'styled-components';
import Algorithms from '../../../../core/utils/algortithms';
import Button from '../../commons/Button/Button';
import { updatePlayers } from '../../../../core/actions/playerActions';
import PlayerIcon from '../../Stats/PlayerStats/LevelStats/PlayerStats/PlayerData/PlayerIcon/PlayerIcon';

const PlayerSettingsContainer = styled.section`
    width: 100%;
    display: flex;
    justify-content: space-between;
    margin: 10px;
    padding: 10px;
    margin-bottom: 2vw;
`;

const InputGroup = styled.div`
    display: flex;
    flex-direction: column;
    justify-items: center;
    align-items: center;
    margin: 10px;
    padding: 5px;
    border-bottom: 5px dotted coral;
`;

const Select = styled.select`
    background-color: rgba(255,255,255, 0.5);
    font-size: 2vw;
    font-weight: bold;
    text-align: center;
`;

const StyledForm = styled.form`
    margin-top: 5vw;
    margin-bottom: 5vw;
`;

const PlayerSettings = () => {
	const { players } = useSelector(state => ({
		players: state.players.players,
	}));

	const [playerSettings, setPlayerSettings] = useState({ players });

	const dispatch = useDispatch();

	const handleAlgorithmChange = (event, player) => {
		const { value } = event.target;

		setPlayerSettings(prevState => ({
			...prevState,
			players: prevState.players.map(p => (p.num === player.num ? { ...p, algorithm: value } : p)),
		}));
	};

	const togglePlayerEnabled = (player) => {
		setPlayerSettings(prevState => ({
			...prevState,
			players:
                prevState.players.map(p => (p.num === player.num ? { ...p, enabled: !prevState.players[player.num - 1].enabled } : p)),
		}));
	};

	const handleSubmit = (event) => {
		event.preventDefault();
		dispatch(updatePlayers(playerSettings.players));
	};

	return (
		<StyledForm onSubmit={handleSubmit}>
			<InputGroup>
				{playerSettings.players.map(p => (
					<PlayerSettingsContainer key={p.num}>
						<PlayerIcon playerNumber={p.num} width={7} height={7} color={p.color} />
						<Select
							defaultValue={p.algorithm}
							onChange={e => handleAlgorithmChange(e, p)}
							id={p.num}
							name={p.num}
						>
							<option value={Algorithms.DIJKSTRA}>Dijkstra</option>
							<option value={Algorithms.ASTAR}>AStart</option>
							<option value={Algorithms.BREADTHFIRST}>BreadthFirst</option>
							<option value={Algorithms.DEPTHFIRST}>DepthFirst</option>
						</Select>
						<Button
							warning={p.enabled}
							dark={!p.enabled}
							type="button"
							text={p.enabled ? 'Disable' : 'Enable'}
							onClick={() => togglePlayerEnabled(p)}
						/>
					</PlayerSettingsContainer>
				))}
			</InputGroup>
			<Button text="Apply Players Changes" primary type="submit" />
		</StyledForm>
	);
};

export default PlayerSettings;

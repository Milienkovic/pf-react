import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import playerAvatarIcon from '../../../../../../../../assets/images/avatar-ring.png';

const PlayerIconContainer = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    color: white;
    border-radius: 50%;
    background-color: ${props => (props.background ? props.background : 'gold')};
    width:${props => (props.width ? props.width : 9)}vw;
    height:${props => (props.height ? props.height : 9)}vw;
`;

const PlayerNumber = styled.span`
    position: absolute;
    font-size: ${props => (props.fontSize ? props.fontSize : 6)}vw;
    font-weight: bold;
`;

const Icon = styled.img`
    width: ${props => (props.width ? props.width + 4 : 13)}vw;
    height: ${props => (props.height ? props.height + 4 : 13)}vw;
`;

const PlayerIcon = (props) => {
	const {
		playerNumber, width, height, color,
	} = props;

	const mountIcon = (iconSource, w, h) => {
		if (w && h) {
			return <Icon src={iconSource} width={w} height={h} />;
		}
		return <Icon src={iconSource} />;
	};
	return (
		<PlayerIconContainer width={width} height={height} background={color}>
			<PlayerNumber fontSize={width * 0.5}>{playerNumber}</PlayerNumber>
			{mountIcon(playerAvatarIcon, width, height)}
		</PlayerIconContainer>
	);
};

PlayerIcon.propTypes = {
	playerNumber: PropTypes.number,
	width: PropTypes.number,
	height: PropTypes.number,
	color: PropTypes.string,
};

PlayerIcon.defaultProps = {
	playerNumber: 1,
	width: 13,
	height: 13,
	color: 'gold',
};

export default PlayerIcon;

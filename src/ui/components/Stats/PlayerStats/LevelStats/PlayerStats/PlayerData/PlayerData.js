import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import timeIcon from '../../../../../../../assets/images/time.png';
import spyglassIcon from '../../../../../../../assets/images/spyglass.png';
import gridIcon from '../../../../../../../assets/images/grid.png';
import PlayerIcon from './PlayerIcon/PlayerIcon';

const PlayerStatsContainer = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-around;
    font-size: 2vw;
    margin: 10px;
    padding: 10px;
    align-items: center;
`;

const Icon = styled.img`
    width: ${props => (props.width ? props.width : '4vw')};
    height: ${props => (props.height ? props.height : '4vw')};
`;

const StyledPlayerData = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 4vw;
    font-weight: bold;
`;

const Span = styled.span`
    display: flex;
    justify-content: center;
    align-items: center;
`;
const PlayerData = (props) => {
	const { player, actions } = props;
	const {
		num, color, time, visitedNodes,
	} = player;

	const mountIcon = (iconSource, width, height) => {
		if (width && height) {
			return <Icon src={iconSource} width={width} height={height} />;
		}
		return <Icon src={iconSource} />;
	};
	return (
		<PlayerStatsContainer>
			<PlayerIcon playerNumber={num} width={7} height={7} color={color} />
			<StyledPlayerData>
				{mountIcon(timeIcon)}
				{' '}
				<Span>
					{time}
					{' '}
					ms
				</Span>
			</StyledPlayerData>
			<StyledPlayerData>
				{mountIcon(spyglassIcon)}
				{' '}
				<Span>
					{visitedNodes.length}
					{' '}
                    &nbsp;
					{' '}
					{mountIcon(gridIcon)}
				</Span>
			</StyledPlayerData>
			{actions}
		</PlayerStatsContainer>
	);
};

PlayerData.propTypes = {
	player: PropTypes.shape({
		num: PropTypes.number,
		color: PropTypes.string,
		time: PropTypes.number,
		visitedNodes: PropTypes.arrayOf(PropTypes.object),
	}),
	actions: PropTypes.element,
};
PlayerData.defaultProps = {
	player: undefined,
	actions: undefined,
};

export default PlayerData;

/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../../../../commons/Button/Button';
import Modal from '../../../../commons/Modal/Modal';
import Node from '../../../../Grid/Node/Node';
import { animateShortestPath } from '../../../../../../core/algorithms/animations/algorithmAnimations';
import useComponentDimensions from '../../../../../../core/hooks/useComponentDimensions';
import { setDimensions } from '../../../../../../core/actions/generalActions';
import replayIcon from '../../../../../../assets/images/replay.png';
import closeIcon from '../../../../../../assets/images/close.png';
import PlayerData from './PlayerData/PlayerData';

const PlayerStatsContainer = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-around;
    font-size: 2vw;
    margin: 10px;
    padding: 10px;
    align-items: center;
`;

const Maze = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: ${props => props.width}px;
    height: ${props => props.height}px;
`;

const ModalActionsContainer = styled.div`
    display: flex;
    align-items: center;
    width:100%;
    justify-content: space-evenly;
`;

const Icon = styled.img`
    width: ${props => (props.width ? props.width : '4vw')};
    height: ${props => (props.height ? props.height : '4vw')};
`;

const PlayerStats = (props) => {
	const { level, player } = props;

	const [display, setDisplay] = useState(false);
	const [disableReplay, setDisableReplay] = useState(false);
	const { width } = useSelector(state => ({
		width: state.general.dimensions.width * 0.9,
	}));

	const containerRef = useRef();
	const { width: containerWidth, height } = useComponentDimensions(containerRef);

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(setDimensions({ width, height }));
	}, [containerWidth, height]);

	const handleLevelReplay = () => {
		setDisplay(true);
	};

	const handleModalClosing = () => {
		setDisableReplay(false);
		setDisplay(false);
	};

	const handleReplayAnimation = () => {
		setDisableReplay(true);
		// animateAlgorithm(player.visitedNodes, player.shortestPathNodes, player.color, true);
		animateShortestPath(player.shortestPathNodes, player.color, true);
	};

	const mountIcon = (iconSource, w, h) => {
		if (w && h) {
			return <Icon src={iconSource} width={w} height={h} />;
		}
		return <Icon src={iconSource} />;
	};

	const mountFooterActions = () => (
		<ModalActionsContainer>
			<Button text={mountIcon(closeIcon)} onClick={handleModalClosing} dark type="button" />
			<Button
				text={mountIcon(replayIcon)}
				onClick={handleReplayAnimation}
				type="button"
				disabled={disableReplay}
			/>
		</ModalActionsContainer>
	);

	const mountPlayerActions = () => <Button text={mountIcon(replayIcon)} onClick={handleLevelReplay} type="button" />;

	return (
		<PlayerStatsContainer ref={containerRef}>
			<PlayerData
				player={player}
				actions={mountPlayerActions()}
			/>
			<Modal
				display={display}
				header={`Level ${level.level} replay`}
				footer={mountFooterActions()}
				style={{ width: width + 2 }}
			>
				<Maze width={width + 2} height={width + 2}>
					{level.maze.map(row => row.map(node => (
						<Node
							key={`row-${node.row}-col${node.col}`}
							node={node}
						/>
					)))}
				</Maze>
			</Modal>
		</PlayerStatsContainer>

	);
};

PlayerStats.propTypes = {
	level: PropTypes.shape({
		level: PropTypes.number,
		maze: PropTypes.arrayOf(PropTypes.array),
		success: PropTypes.bool,
		players: PropTypes.arrayOf(PropTypes.object),

	}),
	player: PropTypes.shape({
		num: PropTypes.number,
		color: PropTypes.string,
		time: PropTypes.number,
		shortestPathNodes: PropTypes.arrayOf(PropTypes.object),
	}),
};

PlayerStats.defaultProps = {
	player: undefined,
	level: undefined,
};

export default PlayerStats;

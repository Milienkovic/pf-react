import React from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';
import PlayerStats from './LevelStats/PlayerStats/PlayerStats';

const LevelStatsContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 100%;
    margin-top: 5rem;
    margin-bottom: 10vh;
    padding-bottom: 10vh;
    background-color: rgba(255, 255, 255, 0.5);
    border-radius: 0 0 5rem 5rem;
`;

const Header = styled.h2`
    color: ${props => (props.color ? props.color : 'black')};
    font-size: 6vw;
`;

const NoStats = styled.div`
    font-size: 4vw;
    color: red;
`;

const LevelStats = () => {
	const levelId = useParams().id;
	const { level, levelsCount } = useSelector(state => ({
		level: state.gameControls.levels.find(l => l.level === +levelId),
		levelsCount: state.gameControls.levels.length,
	}));

	const renderPlayersStats = () => {
		if (levelsCount > 0) {
			return (
				<>
					{level.players.map(player => (
						<PlayerStats
							level={level}
							player={player}
							key={`${player.num}-${level.level}`}
						/>
					))}
				</>
			);
		}
		return <NoStats>No games player yet! Please start the game first!</NoStats>;
	};
	const renderLevels = () => {
		if (level) {
			return (
				<LevelStatsContainer>
					<Header>
						Level
						{level.level}
					</Header>
					{renderPlayersStats()}
				</LevelStatsContainer>
			);
		}
		return <NoStats>No data available!</NoStats>;
	};

	return (
		<>
			{renderLevels()}
		</>
	);
};

export default LevelStats;

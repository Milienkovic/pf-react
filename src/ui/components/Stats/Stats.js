import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import levelIcon from '../../../assets/images/level-icon.png';

const StatsContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-wrap: wrap;
    font-size: 5vw;
    font-weight: bold;
    width: 80%;
    margin: 2vw;
    padding: 1rem;
    background-color: rgba(255,255,255,0.5);
    border-radius: 1rem;
`;

const LevelLink = styled(Link)`
    padding: 1rem;
    margin: 10px;
    text-decoration: none;
    color: darkred;
    
    &:hover{
        cursor: pointer;
        color: red;
    }
`;

const NoData = styled.span`
    margin: 2rem;
    font-size 3vw;
    color: red
`;
const LevelImageContainer = styled.div`
     position: relative;
     display: flex;
     justify-content: center;
     align-items: center;
`;

const Level = styled.span`
    position: absolute;
`;
const LevelImage = styled.img`
    width: 20vw;
    height: 20vw;
`;

const Stats = () => {
	const { levels } = useSelector(state => ({
		levels: state.gameControls.levels,
	}));

	const mountLevelIcon = level => (
		<LevelImageContainer>
			<LevelImage src={levelIcon} />
			<Level>{level}</Level>
		</LevelImageContainer>
	);

	const renderLevels = () => {
		if (levels.length > 0) {
			return (
				<StatsContainer>
					{levels.map(level => (
						<div key={level.level}>
							<LevelLink to={`/levels/${level.level}`}>
								{mountLevelIcon(level.level)}
							</LevelLink>
						</div>
					))}
				</StatsContainer>
			);
		}
		return <NoData>No data available! Please play the game first</NoData>;
	};

	return (
		<>
			{renderLevels()}
		</>
	);
};

export default Stats;

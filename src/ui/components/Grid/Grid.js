/* eslint-disable react-hooks/exhaustive-deps,no-plusplus */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import Node from './Node/Node';
import useMaze from '../../../core/hooks/useMaze';
import GridControls from './GridControls/GridControls';
import {
	autoPlayData,
	autoPlaying,
	manualPlaying,
	resetLevelsData,
	setLevel,
	updateLevelsData,
} from '../../../core/actions/gameControllsActions';
import Modal from '../commons/Modal/Modal';
import Button from '../commons/Button/Button';
import statsIcon from '../../../assets/images/stats.jpg';
import PlayerData from '../Stats/PlayerStats/LevelStats/PlayerStats/PlayerData/PlayerData';
import Paths from '../../../core/utils/paths';
import { animateShortestPath } from '../../../core/algorithms/animations/algorithmAnimations';

const GridContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const Maze = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: ${props => props.width}px;
    height: ${props => props.height}px;
`;

const ModalActionsContainer = styled.div`
    display: flex;
    align-items: center;
    width:100%;
    justify-content: space-evenly;
`;

const ModalBody = styled.div`
    padding: 10px;
    font-size: 2.5vw;
`;

export default function Grid() {
	const [gridState, setGridState] = useState({
		displayModal: false,
		isRunSuccessful: false,
		playersStats: [],
		processing: false,
		isAutoPlay: false,
		isGameOver: false,
		isPlaying: false,
		isStart: true,
	});

	const {
		rows, startNode, finishNode, level, width, players,
	} = useSelector(state => ({
		rows: state.gameControls.rows,
		cols: state.gameControls.cols,
		level: state.gameControls.level,
		startNode: state.gameControls.startNode,
		finishNode: state.gameControls.finishNode,
		width: state.general.dimensions.width * 0.9,
		height: state.general.dimensions.height,
		maze: state.gameControls.maze,
		players: state.players.players.filter(p => p.enabled),
	}));

	const dispatch = useDispatch();
	const history = useHistory();

	const {
		state: mazeState,
		executeAlgorithm2,
		generateMazeWalls,
		createMazeWalls,
		generateMaze,
		executeAlgorithm,
		resetGrid,
		createMaze,
		initMaze,
	} = useMaze(startNode, finishNode, rows, rows, level - 1,);
	// matrix

	useEffect(() => {
		if (!gridState.isAutoPlay) {
			generateMaze();
		}
	}, [mazeState.walls, gridState.isAutoPlay]);

	useEffect(() => {
		if (!gridState.isAutoPlay) {
			generateMazeWalls();
		}
	}, [level, gridState.isAutoPlay]);

	useEffect(() => {
		if (gridState.isAutoPlay) {
			setGridState(prevState => ({
				...prevState,
				isGameOver: false,
			}));
		}
	}, [gridState.isAutoPlay]);

	useEffect(() => {
		dispatch(manualPlaying(gridState.isPlaying));
	}, [gridState.isPlaying]);

	useEffect(() => {
		dispatch(autoPlaying(gridState.isAutoPlay));
	}, [gridState.isAutoPlay]);

	useEffect(() => {
		if (gridState.isGameOver) {
			dispatch(setLevel(1));

			setGridState(prevState => ({
				...prevState,
				playersStats: [],
			}));
		}
	}, [gridState.isGameOver]);

	const playerData = (num, algorithm, color, enabled, playerResult) => ({
		num,
		algorithm,
		color,
		enabled,
		time: playerResult.time,
		visitedNodes: playerResult.visitedNodes,
		shortestPathNodes: playerResult.shortestPathNodes,
	});

	const autoPlay = () => {
		setGridState(prevState => ({
			...prevState,
			isAutoPlay: true,
			processing: true,
		}));

		setTimeout(() => {
			let success = true;
			let lvl = 1;
			const levelsList = [];
			while (success) {
				const initGrid = initMaze(rows, rows, startNode, finishNode);
				const walls = createMazeWalls(rows, rows, initGrid, lvl - 1);
				const maze = createMaze(walls, initGrid);
				const playersList = [];

				for (let i = 0; i < players.length; i++) {
					const {
						num, algorithm, color, enabled,
					} = players[i];
					const playerResult = executeAlgorithm2(players[i].algorithm, maze, startNode, finishNode);
					success = success && playerResult.success;
					playersList.push(playerData(num, algorithm, color, enabled, playerResult));
				}

				const levelData = {
					// eslint-disable-next-line no-plusplus
					level: lvl++,
					success,
					maze,
					players: playersList,
				};
				levelsList.push(levelData);
			}

			if (!success) {
				setGridState(prevState => ({
					...prevState,
					isAutoPlay: false,
					processing: false,
					isGameOver: true,
				}));
			}
			dispatch(autoPlayData(levelsList));
		}, 2000);
	};

	const manageManualPlaying = () => {
		if (gridState.isStart) {
			dispatch(resetLevelsData());
			setGridState(prevState => ({
				...prevState,
				isStart: false,
			}));
		}
		setGridState(prevState => ({
			...prevState,
			processing: true,
			isPlaying: true,
		}));

		const statistics = [];
		let success = true;
		const playersList = [];
		let animated = true;

		for (let i = 0; i < players.length; i++) {
			const {
				num, algorithm, color, enabled,
			} = players[i];

			const playerResults = executeAlgorithm(algorithm, color, true);

			if (playerResults.success) {
				animated = animated && animateShortestPath(playerResults.shortestPathNodes, color, true);
			}
			playersList.push(playerData(num, algorithm, color, enabled, playerResults));

			statistics.push({ ...playerResults, ...players[i] });
			success = success && playerResults.success;
		}

		if (animated) {
			setTimeout(() => {
				setGridState(prevState => ({
					...prevState,
					displayModal: true,
				}));
			}, 2000);
		}

		const levelD = {
			level,
			maze: mazeState.grid,
			success,
			players: playersList,
		};
		dispatch(updateLevelsData(levelD));

		setGridState(prevState => ({
			...prevState,
			isRunSuccessful: success,
			playersStats: statistics,
			isPlaying: success,
		}));

		if (!success) {
			setGridState(prevState => ({
				...prevState,
				isGameOver: true,
			}));
		}
	};

	const generateNewMap = () => {
		generateMazeWalls();
	};

	const handleLevelIncrease = () => {
		resetGrid();

		if (gridState.isRunSuccessful) {
			dispatch(setLevel(level + 1));
		} else if (gridState.isGameOver) {
			history.push(Paths.STATS);
		}

		setGridState(prevState => ({
			...prevState,
			reset: true,
			displayModal: false,
		}));
	};

	const buttonText = () => {
		if (gridState.isRunSuccessful) {
			return 'Next';
		} if (gridState.isGameOver) {
			return <img src={statsIcon} width="40%" height="40%" alt="stats" />;
		}
		return 'New Game';
	};

	const mountModalActions = () => (
		<ModalActionsContainer>
			<Button
				text={buttonText()}
				type="button"
				onClick={handleLevelIncrease}
			/>
		</ModalActionsContainer>
	);

	const mountModalBody = () => {
		const { playersStats } = gridState;

		if (gridState.isGameOver) {
			return (
				<ModalBody>
					<span>Auto play is over click to see statistics</span>
				</ModalBody>
			);
		}

		return playersStats.map(player => (
			<ModalBody key={player.num}>
				<PlayerData player={player} />
			</ModalBody>
		));
	};

	const modalHeader = () => {
		if (gridState.isRunSuccessful) {
			return `Level ${level} completed!`;
		}
		if (gridState.isGameOver) {
			return 'Auto play is finished!';
		}

		return `Level ${level} failed!`;
	};

	return (
		<GridContainer>
			<GridControls
				manualPlaying={manageManualPlaying}
				resetGrid={resetGrid}
				generateMazeWalls={generateNewMap}
				auto={autoPlay}
				autoPlay={gridState.isAutoPlay}
				play={gridState.isPlaying}
				level={level}
			/>
			<Maze width={width + 2} height={width + 2}>
				{mazeState.grid.map(row => row.map(node => (
					<Node
						key={`${node.row}-${node.col}`}
						node={node}
					/>
				)))}
			</Maze>
			<Modal
				display={(gridState.displayModal) || (!gridState.isAutoPlay && gridState.isGameOver)}
				footer={mountModalActions()}
				header={modalHeader()}
			>
				{mountModalBody()}
			</Modal>
		</GridContainer>
	);
}

import React from 'react';
import { useSelector } from 'react-redux';
import styled, { css, keyframes } from 'styled-components';
import PropTypes from 'prop-types';
import treasure from '../../../../assets/images/treasure.ico';
import startPoint from '../../../../assets/images/start.png';
import wall from '../../../../assets/images/block.png';

import './Node.css';

const iconAnimation = keyframes`
    0% {
        transform: scale(0.5);
    }

    50% {
        transform: scale(1.5);
    }

    100% {
        transform: scale(1);
    }
`;

const wallAnimation = keyframes`
    0% {
        transform: scale(0.1);
    }
    50% {
        transform: scale(1.5);
    }
    100% {
        transform: scale(1);
    }
`;

const wallStyle = 'background-color: rgba(60, 60, 60, 0.5);';
const lastRowStyle = 'border-bottom: 1px solid rgba(0,0,0,0.6);';
const lastColStyle = 'border-right: 1px solid rgba(0,0,0,0.6);';

const Icon = styled.div`
    display: flex;
    animation-name: ${props => (props.isWall ? wallAnimation : iconAnimation)};
    animation-duration: 0.4s;
    ${props => (props.isWall ? css`${wallStyle}` : '')}
`;

const StyledNode = styled.div`
	.Node{
		display: flex;
		align-items: center;
		justify-content: center;
		border-left: 1px solid rgba(0,0,0,0.6);
		border-top: 1px solid rgba(0,0,0,0.6);
		animation-duration: 0.4s;
		width: ${props => props.width}px;
		height: ${props => props.height}px;
	}
   
    
    ${props => (props.isLastRow ? css`${lastRowStyle}` : '')}
    ${props => (props.isLastCol ? css`${lastColStyle}` : '')}
    `;

export default function Node(props) {
	const { node } = props;
	const {
		row, col, isStart, isFinish, isWall, lastRow, lastCol,
	} = node;

	const { width, rows: rowsNumber } = useSelector(state => ({
		width: state.general.dimensions.width * 0.9,
		rows: state.gameControls.rows,
	}));

	const nodeWidth = width / rowsNumber - 1;
	const nodeHeight = width / rowsNumber - 1;

	const mountIcon = () => {
		if (isStart && isFinish) {
			return (
				<Icon>
					<img
						src={startPoint}
						width={nodeWidth * 0.33}
						height={nodeHeight * 0.33}
						alt="start"
					/>
					<img
						src={treasure}
						width={nodeWidth * 0.33}
						height={nodeHeight * 0.33}
						alt="treasure"
					/>
				</Icon>
			);
		}

		if (isStart) {
			return (
				<Icon>
					<img
						src={startPoint}
						width={nodeWidth * 0.8}
						height={nodeHeight * 0.8}
						alt="start"
					/>
				</Icon>
			);
		}

		if (isFinish) {
			return (
				<Icon>
					<img
						src={treasure}
						width={nodeWidth * 0.8}
						height={nodeHeight * 0.8}
						alt="treasure"
					/>
				</Icon>
			);
		}

		if (isWall) {
			return (
				<Icon isWall={isWall}>
					<img
						src={wall}
						width={nodeWidth}
						height={nodeHeight}
						alt="wall"
					/>
				</Icon>
			);
		}
		return <></>;
	};

	return (
		<StyledNode
			isWall={isWall}
			isLastRow={lastRow}
			isLastCol={lastCol}
			style={{
				width: nodeWidth, height: nodeHeight, borderLeft: '1px solid black', borderTop: '1px solid black',
			}}
			id={`node-${row}-${col}`}
		>
			{mountIcon()}
		</StyledNode>
	);
}

Node.propTypes = {
	node: PropTypes.shape({
		row: PropTypes.number,
		col: PropTypes.number,
		isWall: PropTypes.bool,
		isStart: PropTypes.bool,
		isFinish: PropTypes.bool,
		lastRow: PropTypes.bool,
		lastCol: PropTypes.bool,
	}),
};

Node.defaultProps = {
	node: undefined,
};

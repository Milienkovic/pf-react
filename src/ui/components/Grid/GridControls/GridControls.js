import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../../commons/Button/Button';
import playIcon from '../../../../assets/images/play.png';
import autoPlayIcon from '../../../../assets/images/autoplay.png';

const GridControlsContainer = styled.section`
    z-index:2;
    display: flex;
    width: 100%;
    justify-content: center;
    margin: 1rem;
    padding: 1rem;
    position: sticky;
    top: 10%;
    background-color: rgba(255,255,255, 0.5);
`;

const Spacer = styled.span`
    flex:1;
`;

const Level = styled.span`
    flex: 1;
    font-size: 3vw;
    color: green;
    font-weight: bold;
    display: flex;
    justify-content: center
`;

const PlayImage = styled.img`
    width: 4vw;
    height: 4vw;
`;

const GridControls = (props) => {
	const {
		manualPlaying, level, auto, autoPlay, play,
	} = props;
	const disabled = autoPlay || play;

	const mountImage = imageSource => <PlayImage src={imageSource} />;

	return (
		<GridControlsContainer>
			<Button
				text={mountImage(autoPlayIcon)}
				onClick={auto}
				disabled={disabled}
			/>
			<Spacer />
			<Level>
				Level &nbsp;
				{level}
			</Level>
			<Spacer />
			<Button
				text={mountImage(playIcon)}
				onClick={manualPlaying}
				primary
				disabled={autoPlay}
			/>
		</GridControlsContainer>
	);
};

GridControls.propTypes = {
	autoPlay: PropTypes.bool,
	play: PropTypes.bool,
	level: PropTypes.number,
	manualPlaying: PropTypes.func,
	auto: PropTypes.func,
};

GridControls.defaultProps = {
	autoPlay: false,
	play: false,
	level: 1,
	manualPlaying: undefined,
	auto: undefined,
};

export default GridControls;

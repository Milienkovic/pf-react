import {
	heuristic, sortNodesByCost, getNeighborsQueue, checkOpenList,
} from './utils';

export default function aStar(grid, start, end) {
	const startTime = performance.now();
	start.distanceToStart = 0;
	start.cost = heuristic(start, end);
	let openNodes = [start];
	const visitedNodes = [];

	while (openNodes.length) {
		openNodes = sortNodesByCost(openNodes);
		const currentNode = openNodes.shift();

		if (currentNode.col === end.col && currentNode.row === end.row) {
			visitedNodes.push(currentNode);
			return { visitedNodes, time: performance.now() - startTime, success: true };
		}

		const neighbors = getNeighborsQueue(currentNode, grid);
		for (let i = 0; i < neighbors.length; i += 1) {
			const neighbor = neighbors[i];
			const heuristicToEnd = heuristic(neighbor, end);

			const isNeighborAvailable = !neighbor.isVisited && !neighbor.isWall;
			if (isNeighborAvailable) {
				neighbor.distanceToStart = currentNode.distanceToStart + 1;
				neighbor.heuristic = heuristicToEnd;

				if (neighbor.isWeight) {
					neighbor.heuristic += 2;
				}

				if (currentNode.isWeight && neighbor.isWeight) {
					neighbor.heuristic *= 1.5;
				}

				neighbor.cost = neighbor.distanceToStart + neighbor.heuristic;

				if (checkOpenList(openNodes, neighbor)) {
					openNodes.push(neighbor);
					neighbor.previousNode = currentNode;
				}
			}
		}

		currentNode.isVisited = true;

		visitedNodes.push(currentNode);
	}

	return { visitedNodes, time: performance.now() - startTime, success: false };
}

import { getNeighborsQueue } from './utils';

export default function breadthFirst(grid, start, end) {
	const startTime = performance.now();
	const queue = [start];
	const visitedNodes = [start];

	while (queue.length > 0) {
		const currentNode = queue.shift();
		currentNode.isVisited = true;
		visitedNodes.push(currentNode);

		if (currentNode === end) {
			return { visitedNodes, success: true, time: performance.now() - startTime };
		}

		const neighbors = getNeighborsQueue(currentNode, grid);

		neighbors.forEach((neighbor) => {
			if (!neighbor.isVisited) {
				neighbor.previousNode = currentNode;
				if (queue.indexOf(neighbor) < 0) queue.push(neighbor);
			}
		});
	}

	return { visitedNodes, success: false, time: performance.now() - startTime };
}

import { getNeighborsStack } from './utils';

export default function depthFirst(grid, start, end) {
	const startTime = performance.now();
	const stack = [start];
	const visitedNodes = [start];

	while (stack.length > 0) {
		const currentNode = stack.pop();
		visitedNodes.push(currentNode);
		currentNode.isVisited = true;

		if (currentNode === end) {
			return { visitedNodes, success: true, time: performance.now() - startTime };
		}

		const neighbors = getNeighborsStack(currentNode, grid);

		neighbors.forEach((neighbor) => {
			if (!neighbor.isVisited) {
				neighbor.previousNode = currentNode;
				stack.push(neighbor);
			}
		});
	}

	return { visitedNodes, time: performance.now() - startTime, success: false };
}

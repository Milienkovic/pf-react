import { removeNestedNodes, sortNodesByDistance, updateUnvisitedNeighbors } from './utils';

export default function dijkstra(grid, startNode, finishNode) {
	const startTime = performance.now();
	const visitedNodes = [];
	startNode.distance = 0;
	const openNodes = removeNestedNodes(grid);

	while (openNodes.length > 0) {
		sortNodesByDistance(openNodes);
		const closestNode = openNodes.shift();

		if (closestNode.isWall) continue;

		if (closestNode.distance === Infinity) {
			return {
				visitedNodes,
				time: performance.now() - startTime,
				success: false,
			};
		}

		closestNode.isVisited = true;
		visitedNodes.push(closestNode);

		if (closestNode.row === finishNode.row && closestNode.col === finishNode.col) {
			return {
				visitedNodes,
				time: performance.now() - startTime,
				success: true,
			};
		}

		updateUnvisitedNeighbors(closestNode, grid);
	}

	return {
		visitedNodes,
		time: performance.now() - startTime,
		success: false,
	};
}

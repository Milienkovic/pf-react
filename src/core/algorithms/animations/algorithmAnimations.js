/* eslint-disable no-plusplus */
import { getShortestPathNodes } from '../utils';

const bgImagePosition = (color) => {
	let position = 'center center';
	if (color === 'green') {
		position = 'top left';
	}
	if (color === 'gold') {
		position = 'top right';
	}
	if (color === 'blue') {
		position = 'bottom right';
	}
	if (color === 'red') {
		position = 'bottom left';
	}

	return position;
};

const animateVisitedNode = (visitedNode, index,) => {
	setTimeout(() => {
		const currentNodeElem = document.getElementById(`node-${visitedNode.row}-${visitedNode.col}`);

		if (visitedNode.lastRow) {
			currentNodeElem.className += ' node-first-visited-last-row';
		}

		if (visitedNode.lastCol) {
			currentNodeElem.className += ' node-first-visited-last-col';
		}

		currentNodeElem.className += ' node-first-visited';
	}, 10 * index);
};

export const animateShortestPath = (shortestPathNodes, color, animate) => {
	let animated = false;

	if (animate) {
		for (let i = 0; i < shortestPathNodes.length; i++) {
			setTimeout(() => {
				const node = shortestPathNodes[i];
				const currentNodeElem = document.getElementById(`node-${node.row}-${node.col}`);

				if (!node) return;

				if (node.lastRow) {
					currentNodeElem.className += ' node-shortest-path-last-row';
				}

				if (node.lastCol) {
					currentNodeElem.className += ' node-shortest-path-last-col';
				}
				currentNodeElem.className += ' node-shortest-path';
				currentNodeElem.style.backgroundColor = color;
				currentNodeElem.style.opacity = '0.8';
				// currentNodeElem.style.backgroundImage = `url(${ship})`
				currentNodeElem.style.backgroundPosition += `${bgImagePosition(color)}`;
			}, 10 * i,);

			if (i === shortestPathNodes.length - 1) {
				animated = true;
			}
		}
	}
	return animated;
};

export const animateAlgorithm = (visitedNodesInOrder, shortestPathNodes, color, animate) => {
	if (animate) {
		for (let i = 0; i <= visitedNodesInOrder.length; i++) { // once all nodes are animated, animate the shortest path
			const node = visitedNodesInOrder[i];

			if (i === visitedNodesInOrder.length) {
				setTimeout(() => {
					animateShortestPath(shortestPathNodes, color, animate);
				}, 10 * i);
			} else {
				animateVisitedNode(node, i);
			}
		}
	}
};

export default function visualizeAlgorithm(algorithm, grid, startNode, finishNode, color, animate) {
	const firstGrid = grid.map(row => row.map(node => ({
		...node,
		isVisited: false,
	})));

	const startNodeObj = firstGrid[startNode.row][startNode.col];
	const finishNodeObj = firstGrid[finishNode.row][finishNode.col];

	const result = algorithm(firstGrid, startNodeObj, finishNodeObj);
	const shortestPathNodes = getShortestPathNodes(startNodeObj, finishNodeObj);

	animateShortestPath(shortestPathNodes, color, animate);
	return { ...result, shortestPathNodes };
}

/* eslint-disable no-plusplus,no-restricted-syntax,no-restricted-properties */
export const heuristic = (currentNode, endNode) => {
	const differenceInCol = Math.pow(currentNode.col - endNode.col, 2);
	const differenceInRow = Math.pow(currentNode.row - endNode.row, 2);
	return Math.sqrt(differenceInCol + differenceInRow);
};

export const getNeighborsQueue = (node, grid) => {
	const neighbors = [];

	const { row, col } = node;

	if (grid[row - 1] && !grid[row - 1][col].isWall) neighbors.push(grid[row - 1][col]);

	if (grid[row][col + 1] && !grid[row][col + 1].isWall) neighbors.push(grid[row][col + 1]);

	if (grid[row + 1] && !grid[row + 1][col].isWall) neighbors.push(grid[row + 1][col]);

	if (grid[row][col - 1] && !grid[row][col - 1].isWall) neighbors.push(grid[row][col - 1]);

	return neighbors;
};

export const getNeighborsStack = (node, grid) => {
	const neighbors = [];

	const { row, col } = node;
	if (grid[row - 1] && !grid[row - 1][col].isWall) neighbors.unshift(grid[row - 1][col]);

	if (grid[row][col + 1] && !grid[row][col + 1].isWall) neighbors.unshift(grid[row][col + 1]);

	if (grid[row + 1] && !grid[row + 1][col].isWall) neighbors.unshift(grid[row + 1][col]);

	if (grid[row][col - 1] && !grid[row][col - 1].isWall) neighbors.unshift(grid[row][col - 1]);

	return neighbors;
};

export const updateUnvisitedNeighbors = (node, grid) => {
	const unvisitedNeighbors = getNeighborsQueue(node, grid).filter(neighbor => !neighbor.isVisited);

	for (const neighbor of unvisitedNeighbors) {
		neighbor.distance = node.distance + 1;
		neighbor.previousNode = node;
	}
};

export const sortNodesByDistance = (unvisitedNodes) => {
	unvisitedNodes.sort((node1, node2) => node1.distance - node2.distance);
};

export const sortNodesByCost = (nodes) => {
	// eslint-disable-next-line no-param-reassign
	nodes = nodes.sort((node1, node2) => {
		if (node1.cost > node2.cost) return 1;
		if (node1.cost < node2.cost) return -1;
		if (node1.cost === node2.cost) {
			const tiebreaker = 1 / (46 * 16);

			node1.cost = node1.distanceToStart + node1.heuristic * (1.0 + tiebreaker);
			node2.cost = node2.distanceToStart + node2.heuristic * (1.0 + tiebreaker);

			if (node1.cost > node2.cost) return 1;
			if (node1.cost < node2.cost) return -1;
		}
		return 0;
	});

	return nodes;
};

export const removeNestedNodes = (grid) => {
	const nodes = [];

	for (const row of grid) {
		for (const node of row) {
			nodes.push(node);
		}
	}

	return nodes;
};

export const getShortestPathNodes = (startNode, finishNode) => {
	const path = [];
	let currentNode = finishNode;
	while (currentNode && !(startNode.row === currentNode.row && startNode.col === currentNode.col)) {
		path.unshift(currentNode);
		currentNode = currentNode.previousNode;
	}

	path.unshift(currentNode);
	if (!path.includes(startNode)) return [];
	return path;
};

export const checkOpenList = (openList, newNode) => {
	let comparison = true;

	openList.forEach((node) => {
		if (node.row === newNode.row && node.col === newNode.col) {
			if (newNode.cost >= node.cost) {
				comparison = false;
			} else {
				comparison = false;
			}
		}
	});

	return comparison;
};

export const executeAlgo = (algorithm, grid, startNode, finishNode) => {
	const firstGrid = grid.map(row => row.map(node => ({
		...node,
		isVisited: false,
	})));
	const startNodeObj = firstGrid[startNode.row][startNode.col];
	const finishNodeObj = firstGrid[finishNode.row][finishNode.col];

	const result = algorithm(firstGrid, startNodeObj, finishNodeObj);
	const shortestPathNodes = getShortestPathNodes(startNodeObj, finishNodeObj);

	return { ...result, shortestPathNodes };
};

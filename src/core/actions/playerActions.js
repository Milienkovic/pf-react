import PlayerTypes from './types/playerTypes';
import GameControlsTypes from './types/gameControlsTypes';

export const updatePlayers = players => ({
	type: PlayerTypes.UPDATE_PLAYERS,
	players,
});

export const setSuccess = (player, success) => ({
	type: PlayerTypes.SET_SUCCESS,
	success,
	player,
});

export const gameOver = () => ({
	type: GameControlsTypes.GAME_OVER,

});

import GameControlsTypes from './types/gameControlsTypes';

export const setRows = rows => ({
	type: GameControlsTypes.SET_ROWS,
	rows,
});

export const setCols = cols => ({
	type: GameControlsTypes.SET_COLS,
	cols,
});

export const setLevel = level => ({
	type: GameControlsTypes.SET_LEVEL,
	level,
});

export const setStart = start => ({
	type: GameControlsTypes.SET_START,
	start,
});

export const setFinish = finish => ({
	type: GameControlsTypes.SET_FINISH,
	finish,
});

export const manualPlaying = isManualPlaying => ({
	type: GameControlsTypes.MANUAL_PLAYING,
	isManualPlaying,
});

export const autoPlaying = isAutoPlaying => ({
	type: GameControlsTypes.AUTO_PLAYING,
	isAutoPlaying,
});

export const gameOver = gOver => ({
	type: GameControlsTypes.GAME_OVER,
	gameOver: gOver,
});

export const updateLevelsData = level => ({
	type: GameControlsTypes.UPDATE_LEVELS_DATA,
	level,
});

export const resetLevelsData = () => ({
	type: GameControlsTypes.RESET_LEVELS_DATA,
});

export const autoPlayData = data => ({
	type: GameControlsTypes.AUTO_PLAY_DATA,
	data,
});

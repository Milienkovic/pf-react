export default class PlayerTypes {
    static UPDATE_PLAYERS = 'UPDATE_PLAYERS';

    static SET_SUCCESS = 'SET_SUCCESS';

    static SET_PLAYERS_SUCCESS = 'SET_PLAYERS_SUCCESS';

    static UPDATE_PLAYER_GAME_DATA = 'UPDATE_PLAYER_GAME_DATA';
}

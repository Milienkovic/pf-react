export default class GameControlsTypes {
    static SET_ROWS = 'SET_ROWS';

    static SET_COLS = 'SET_COLS';

    static SET_START = 'SET_START';

    static SET_FINISH = 'SET_FINISH';

    static SET_LEVEL = 'SET_LEVEL';

    static UPDATE_LEVELS_DATA = 'UPDATE_LEVELS_DATA';

    static AUTO_PLAY_DATA = 'AUTO_PLAY_DATA';

    static MANUAL_PLAYING = 'MANUAL_PLAYING';

    static AUTO_PLAYING = 'AUTO_PLAYING'

    static RESET_LEVELS_DATA = 'RESET_LEVELS_DATA';

    static GAME_OVER = 'GAME_OVER';
}

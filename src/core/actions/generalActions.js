import GeneralTypes from './types/generalTypes';

// eslint-disable-next-line import/prefer-default-export
export const setDimensions = dimensions => ({
	type: GeneralTypes.SET_DIMENSIONS,
	dimensions,
});

import { useCallback, useEffect, useState } from 'react';

export default function useComponentDimensions(ref) {
	const [dimensions, setDimensions] = useState({ width: 0, height: 0 });

	const getDimensions = useCallback(() => ({ width: ref.current.offsetWidth, height: ref.current.offsetHeight }), [ref]);

	useEffect(() => {
		const handleResize = () => setDimensions(getDimensions());
		if (ref.current) {
			setDimensions(getDimensions);
			window.addEventListener('resize', handleResize);

			return () => window.removeEventListener('resize', handleResize);
		}
		return () => {
			// will unmount
		};
	}, [getDimensions, ref]);

	return dimensions;
}

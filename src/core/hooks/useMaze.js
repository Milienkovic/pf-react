/* eslint-disable no-plusplus,no-mixed-spaces-and-tabs */
import { useState } from 'react';
import dijkstra from '../algorithms/dijkstra';
import depthFirst from '../algorithms/depthFirst';
import breadthFirst from '../algorithms/breadthFirst';
import aStar from '../algorithms/aStar';
import { executeAlgo as execute } from '../algorithms/utils';

export default function useMaze(startingNode, finishingNode, rows, cols, numberOfWalls, oldMaze) {
	const [startNode, setStartNode] = useState({ row: startingNode.row, col: startingNode.col });
	const [finishNode, setFinishNode] = useState({ row: finishingNode.row, col: finishingNode.col });

	const createNode = (row, col, sNode, fNode) => ({
		row,
		col,
		isStart: row === sNode.row && col === sNode.col,
		isFinish: row === fNode.row && col === fNode.col,
		distance: Infinity,
		isVisited: false,
		isWall: false,
		previousNode: null,
		lastRow: row === rows - 1,
		lastCol: col === cols - 1,
		distanceToStart: 0,
		heuristic: 0,
		cost: Infinity,
	});

	const setInitialGrid = () => {
		const grid = [];

		for (let row = 0; row < rows; row++) {
			const currentRow = [];

			for (let col = 0; col < cols; col++) {
				currentRow.push(createNode(row, col, startNode, finishNode));
			}
			grid.push(currentRow);
		}
		return grid;
	};

	const initMaze = (r, c, sNode, fNode) => {
		const grid = [];

		for (let row = 0; row < r; row++) {
			const currentRow = [];

			for (let col = 0; col < c; col++) {
				currentRow.push(createNode(row, col, sNode, fNode));
			}
			grid.push(currentRow);
		}
		return grid;
	};

	const [state, setState] = useState({
		stateRows: rows || 10,
		stateCols: cols || 10,
		walls: [],
		grid: oldMaze || setInitialGrid(),
		inProgress: false,
		algorithm: 'DIJKSTRA',
	});

	const getRandomNode = (grid, possibleRows, possibleCols) => {
		const randomRowIndex = Math.floor(Math.random() * possibleRows.length);
		const randomColIndex = Math.floor(Math.random() * possibleCols.length);

		const currentRow = possibleRows[randomRowIndex];
		const randomCol = possibleCols[randomColIndex];
		return grid[currentRow][randomCol];
	};

	const generateMazeWalls = () => {
		const { stateRows, stateCols } = state;
		setState(prevState => ({ ...prevState, walls: [] }));
		const possibleRows = [];
		for (let i = 0; i < stateRows; i++) {
			possibleRows.push(i);
		}

		const possibleCols = [];
		for (let i = 0; i < stateCols; i++) {
			possibleCols.push(i);
		}

		const walls = new Set();
		while (walls.size < numberOfWalls) {
			const node = getRandomNode(state.grid, possibleRows, possibleCols);
			if (!node.isStart && !node.isFinish) {
				walls.add(node);
			}
		}
		setState(prevState => ({ ...prevState, walls: [...walls] }));
		return walls;
	};

	const loadMaze = (prevMaze) => {
		let maze = [];
		if (prevMaze && prevMaze.length > 0) {
			maze =
                prevMaze.map(row => row.map((node) => {
                	const newNode = { ...node, isWall: false };
                	if (state.walls.includes(node)) newNode.isWall = true;
                	return newNode;
                }));
			setState(prevState => ({ ...prevState, grid: maze }));
			return maze;
		}
		return maze;
	};

	// todo
	const createMazeWalls = (numberOfRows, numberOfCols, maze, wallNumber) => {
		const possibleRows = [];
		for (let i = 0; i < numberOfRows; i++) {
			possibleRows.push(i);
		}

		const possibleCols = [];
		for (let i = 0; i < numberOfCols; i++) {
			possibleCols.push(i);
		}

		const walls = new Set();
		while (walls.size < wallNumber) {
			const node = getRandomNode(maze, possibleRows, possibleCols);
			if (!node.isStart && !node.isFinish) {
				walls.add(node);
			}
		}
		return [...walls];
	};

	// todo
	const createMaze = (walls, initGrid) => initGrid.map(row => row.map((node) => {
		const newNode = {
			...node,
			isWall: false,
		};
		if (walls.includes(node)) newNode.isWall = true;
		return newNode;
	}));

	const generateMaze = (oldM) => {
		let maze = [];
		if (oldM && oldM.length > 0) {
			maze =
                oldM.map(row => row.map((node) => {
                	const newNode = {
                		...node,
                		isWall: false, // ensures grid is refreshed if generateMaze is called multiple times
                	};
                	if (state.walls.includes(node)) newNode.isWall = true;
                	return newNode;
                }));
			// setState(prevState => ({...prevState, grid: maze}));
		} else {
			maze =
                state.grid.map(row => row.map((node) => {
                	const newNode = {
                		...node,
                		isWall: false, // ensures grid is refreshed if generateMaze is called multiple times
                	};
                	if (state.walls.includes(node)) {
                		newNode.isWall = true;
                	}
                	return newNode;
                }));
		}
		setState(prevState => ({ ...prevState, grid: maze }));
		return maze;
	};

	const resetGrid = () => {
		setStartNode({ row: startNode.row, col: startNode.col });
		setFinishNode({ row: finishNode.row, col: finishNode.col });

		setState(prev => ({
			...prev,
			grid: setInitialGrid(),
			inProgress: false,
		}));

		state.grid.forEach((row) => {
			row.forEach((node) => {
				const currentNodeElem = document.getElementById(`node-${node.row}-${node.col}`);
				currentNodeElem.className = 'Node';
				currentNodeElem.style.backgroundColor = 'transparent';

				if (node.lastRow) {
					currentNodeElem.className += ' node-last-row';
					currentNodeElem.style.backgroundColor = 'transparent';
				}

				if (node.lastCol) {
					currentNodeElem.className += ' node-last-col';
					currentNodeElem.style.backgroundColor = 'transparent';
				}
			});
		});
	};
	// todo
	const executeAlgorithm = (algorithm, color, animate) => {
		switch (algorithm) {
		case 'DIJKSTRA':
			return execute(dijkstra, state.grid, startNode, finishNode, color, animate);
		case 'DEPTH-FIRST':
			return execute(depthFirst, state.grid, startNode, finishNode, color, animate);
		case 'BREADTH-FIRST':
			return execute(breadthFirst, state.grid, startNode, finishNode, color, animate);
		case 'A-STAR':
			return execute(aStar, state.grid, startNode, finishNode, color, animate);
		default:
			return execute(dijkstra, state.grid, startNode, finishNode, color, animate);
		}
	};

	// todo algo 2
	const executeAlgorithm2 = (algorithm, maze, strtNode, fnshNode) => {
		if (maze.length > 0) {
			switch (algorithm) {
			case 'DIJKSTRA':
				return execute(dijkstra, maze, strtNode, fnshNode,);
			case 'DEPTH-FIRST':
				return execute(depthFirst, maze, strtNode, fnshNode,);
			case 'BREADTH-FIRST':
				return execute(breadthFirst, maze, strtNode, fnshNode,);
			case 'A-STAR':
				return execute(aStar, maze, strtNode, fnshNode,);
			default:
				return execute(dijkstra, maze, strtNode, fnshNode,);
			}
		}
		return null;
	};

	const clearGrid = (type) => {
		const oldGrid = [...state.grid];

		const grid = oldGrid.map(row => row.map((node) => {
			const newNode = { ...node };

			if (type === 'WALLS') {
				newNode.isWall = false;
			}
			return newNode;
		}));

		setState(prevState => ({ ...prevState, grid }));
	};

	return {
		state,
		generateMazeWalls,
		generateMaze,
		executeAlgorithm,
		resetGrid,
		clearGrid,
		setState,
		loadMaze,
		executeAlgorithm2,
		setInitialGrid,
		createMazeWalls,
		createMaze,
		initMaze,
	};
}

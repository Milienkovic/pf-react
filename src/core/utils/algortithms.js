export default class Algortithms {
    static DIJKSTRA = 'DIJKSTRA';

    static ASTAR = 'A-STAR';

    static BREADTHFIRST = 'BREADTH-FIRST';

    static DEPTHFIRST = 'DEPTH-FIRST';
}

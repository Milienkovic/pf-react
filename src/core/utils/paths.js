export default class Paths {
    static HOME='/';

    static SETTINGS='/settings';

    static STATS='/stats';

    static LEVELS='/levels/:id';

    static levels = levelId => `/levels/${levelId}`;
}

import thunk from 'redux-thunk';
import { logger } from 'redux-logger';
import { applyMiddleware, compose, createStore } from 'redux';
import rootReducer from '../reducers';

const middlewares = [thunk, logger];

const composeEnhancers = process.env.NODE_ENV === 'development'
	? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;
const store = createStore(rootReducer, composeEnhancers(
	applyMiddleware(...middlewares)
));

export default store;

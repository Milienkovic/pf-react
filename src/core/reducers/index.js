import { combineReducers } from 'redux';
import generalReducer from './generalReducer';
import gameControlsReducer from './gameControlsReducer';
import playerReducer from './playerReducers';

const rootReducer = combineReducers({
	general: generalReducer,
	gameControls: gameControlsReducer,
	players: playerReducer,
});
export default rootReducer;

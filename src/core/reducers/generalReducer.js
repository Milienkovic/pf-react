import GeneralTypes from '../actions/types/generalTypes';

const initState = {
	dimensions: { width: 0, height: 0 },

};

const setAvailableDimensions = (state, action) => ({
	...state,
	dimensions: action.dimensions,
});

export default function generalReducer(state = initState, action) {
	switch (action.type) {
	case GeneralTypes.SET_DIMENSIONS:
		return setAvailableDimensions(state, action);
	default:
		return state;
	}
}

import Algortithms from '../utils/algortithms';
import PlayerTypes from '../actions/types/playerTypes';

const initState = {
	players: [
		{
			num: 1, algorithm: Algortithms.DIJKSTRA, color: 'green', enabled: true,
		},
		{
			num: 2, algorithm: Algortithms.ASTAR, color: 'gold', enabled: true,
		},
		{
			num: 3, algorithm: Algortithms.BREADTHFIRST, color: 'blue', enabled: true,
		},
		{
			num: 4, algorithm: Algortithms.DEPTHFIRST, color: 'red', enabled: true,
		},
	],
};

const updatePlayers = (state, action) => ({
	...state,
	players: action.players,
});

const setSuccess = (state, action) => ({
	...state,
	players: state.players.map(p => (p.num === action.player.num ? { ...p, success: action.success } : p)),
});

const setPlayersSuccess = (state, action) => ({
	...state,
	players: action.players,
});

export default function playerReducers(state = initState, action) {
	switch (action.type) {
	case PlayerTypes.UPDATE_PLAYERS:
		return updatePlayers(state, action);
	case PlayerTypes.SET_SUCCESS:
		return setSuccess(state, action);
	case PlayerTypes.SET_PLAYERS_SUCCESS:
		return setPlayersSuccess(state, action);
	default:
		return state;
	}
}

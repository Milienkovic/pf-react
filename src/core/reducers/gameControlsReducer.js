import GameControlsTypes from '../actions/types/gameControlsTypes';

const initState = {
	rows: 10,
	cols: 10,
	startNode: { row: 0, col: 4 },
	finishNode: { row: 9, col: 4 },
	level: 1,
	isManualPlaying: false,
	isAutoPlaying: false,
	levels: [],
};

const setPropAction = (state, propName, propValue) => ({
	...state,
	[propName]: propValue,
});

const setRows = (state, action) => setPropAction(state, 'rows', action.rows);

const setCols = (state, action) => setPropAction(state, 'cols', action.cols);

const setLevel = (state, action) => setPropAction(state, 'level', action.level);

const setStartNode = (state, action) => setPropAction(state, 'startNode', action.start);

const setFinishNode = (state, action) => setPropAction(state, 'finishNode', action.finish);

const manualPlaying = (state, action) => setPropAction(state, 'isManualPlaying', action.isManualPlaying);

const autoPlaying = (state, action) => setPropAction(state, 'isAutoPlaying', action.isAutoPlaying);

const resetLevelsData = state => setPropAction(state, 'levels', []);

const updateLevelsData = (state, action) => ({
	...state,
	levels: [...state.levels, action.level],
});

const autoPlayData = (state, action) => ({
	...state,
	levels: action.data,
});
const gameOver = (state, action) => ({
	...state,
	isPlaying: false,
	level: action.gameOver.level,
});

export default function (state = initState, action) {
	switch (action.type) {
	case GameControlsTypes.SET_ROWS:
		return setRows(state, action);
	case GameControlsTypes.SET_COLS:
		return setCols(state, action);
	case GameControlsTypes.SET_LEVEL:
		return setLevel(state, action);
	case GameControlsTypes.SET_START:
		return setStartNode(state, action);
	case GameControlsTypes.SET_FINISH:
		return setFinishNode(state, action);
	case GameControlsTypes.UPDATE_LEVELS_DATA:
		return updateLevelsData(state, action);
	case GameControlsTypes.AUTO_PLAY_DATA:
		return autoPlayData(state, action);
	case GameControlsTypes.MANUAL_PLAYING:
		return manualPlaying(state, action);
	case GameControlsTypes.AUTO_PLAYING:
		return autoPlaying(state, action);
	case GameControlsTypes.RESET_LEVELS_DATA:
		return resetLevelsData(state, action);
	case GameControlsTypes.GAME_OVER:
		return gameOver(state, action);
	default:
		return state;
	}
}
